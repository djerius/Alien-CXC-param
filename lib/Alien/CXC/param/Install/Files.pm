package Alien::CXC::param::Install::Files;

use v5.10;
use strict;
use warnings;

our $VERSION = '0.04';

require Alien::CXC::param;

sub Inline { shift; Alien::CXC::param->Inline( @_ ) }
1;

=begin Pod::Coverage

  Inline

=cut
