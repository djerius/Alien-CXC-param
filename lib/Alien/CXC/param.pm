package Alien::CXC::param;

# ABSTRACT: Build and Install the CIAO cxcparam library

use v5.12;
use strict;
use warnings;

our $VERSION = '0.04';

use base qw( Alien::Base );

1;

# COPYRIGHT

__END__

=pod

=for stopwords
metacpan
cxcparam

=head1 SYNOPSIS

  use Alien::CXC::param;

=head1 DESCRIPTION

This module finds or builds the I<cxcparam> library extracted from
the Chandra Interactive Analysis of Observations (CIAO) software
package produced by the Chandra X-Ray Center (CXC).
See L<https://cxc.harvard.edu/ciao/> for more information.

The C<cxcparam> library is itself released under separate license.
See the README file in the included distribution tarball.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

