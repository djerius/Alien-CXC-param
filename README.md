# NAME

Alien::CXC::param - Build and Install the CIAO cxcparam library

# VERSION

version 0.04

# SYNOPSIS

    use Alien::CXC::param;

# DESCRIPTION

This module finds or builds the _cxcparam_ library extracted from
the Chandra Interactive Analysis of Observations (CIAO) software
package produced by the Chandra X-Ray Center (CXC).
See [https://cxc.harvard.edu/ciao/](https://cxc.harvard.edu/ciao/) for more information.

The `cxcparam` library is itself released under separate license.
See the README file in the included distribution tarball.

# USAGE

Please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien%3A%3ABuild%3A%3AManual%3A%3AAlienUser) (or equivalently on [metacpan](https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod)).

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-alien-cxc-param@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-CXC-param](https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-CXC-param)

## Source

Source is available at

    https://gitlab.com/djerius/alien-cxc-param

and may be cloned from

    https://gitlab.com/djerius/alien-cxc-param.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2023 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
